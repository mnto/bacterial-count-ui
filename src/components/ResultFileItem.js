import React, { Component } from 'react';
import { Checkbox } from 'semantic-ui-react';
import '../stylesheets/ResultFileItem.scss';

/**
 * each row in the list of downloadable files
 * @props checked bool whether or not the file item is selected
 * @props handleCheck () : void what to do when an item is checked.  toggles value of checked
 */
class ResultFileItem extends Component {
  state = {
    checked: this.props.checked
  }

  /**
   * method that runs when the props update
   * checks for differences and appropriately updates state
   * @param {*} nextProps the incoming props we're updating to
   */
  componentWillReceiveProps(nextProps) {
    if(nextProps.checked !== this.state.checked) {
      this.setState({checked: nextProps.checked});
    }
  }

  render() {
    return (
      <div onClick={this.props.handleCheck} className='file-list-item'>
        <Checkbox label={this.props.filename} onChange={this.props.handleCheck} checked={this.state.checked} />
      </div>
    )
  }
}

export default ResultFileItem;
