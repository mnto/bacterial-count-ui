import React, { Component } from 'react';
import '../stylesheets/Footer.scss';

class Footer extends Component {
  render() {
    return(
      <div className="footer">
        <p className="footer-text">
          HAMILTON COLLEGE BIOLOGY DEPARTMENT |
          HAMILTON COLLEGE COMPUTER SCIENCE DEPARTMENT |
          &copy; 2018
        </p>
      </div>
    );
  }
}

export default Footer;
