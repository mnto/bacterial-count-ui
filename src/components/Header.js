import React, { Component } from 'react';
import '../stylesheets/Header.scss';

class Header extends Component {
  render() {
    return(
      <div className="header">
        <div className="header-logo">
          <p className="header-college">HAMILTON COLLEGE</p>
          <p className="header-dept">BIOLOGY DEPARTMENT</p>
        </div>
      </div>
    );
  }
}

export default Header;
