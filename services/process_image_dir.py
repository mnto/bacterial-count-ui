"""
Label image regions
http://scikit-image.org/docs/dev/auto_examples/segmentation/plot_label.html#sphx-glr-download-auto-examples-segmentation-plot-label-py

This example shows how to segment an image with image labelling. The following
steps are applied:

1. Thresholding with automatic yen method
2. Close small holes with binary closing
3. Measure image regions to filter small objects


TODO:
 investigate regions
 iterate through images
 check with labels

"""

import os, csv, sys, json
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np

from skimage import data, io
from skimage.util import img_as_ubyte
from skimage.filters import threshold_yen, threshold_isodata, threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.color import label2rgb, rgb2gray
from PIL import Image

from skimage.morphology import erosion, dilation, opening, closing, white_tophat
from skimage.morphology import black_tophat, skeletonize, convex_hull_image
from skimage.morphology import disk, square

import wires as wirecounter

# /**
#  * print and flush std out to send output to server
#  * @param data = {
#  *          nProcessed: the number of images processed so far
#  *          nToProcess: the number of images to be processed still
#  *          image: image file / filename
#  *          nCells: cell count
#  *          nSplit: splitting cell count
#  *          nWires: wire count (to ground)
#  *          nC2CWires: cell to cell count
#  *          subregions: array of coordinates defining subregions of uncertainty
#  *        }
#  */
def output(str_obj):
    print(str_obj)
    sys.stdout.flush()

def get_labelled_image(image):
    # apply threshold
    thresh = threshold_yen(image)
    bw = closing(image > thresh, square(3))
    # label image regions
    return label(bw)

def count_regions(image):
    labelled_image = get_labelled_image(image)

    count = 0
    for region in regionprops(labelled_image):
        # take regions with large enough areas
        if region.area >= 1200:
            # and perform a second pass
            count += len(get_cropped_regions(image, region.bbox))
        elif region.area >= 200:
            count += 1

    return count

def crop_image(image, coords):
    """ given an image and coordinates forming a rectangle within the image
        return a cropped subimage.  images are numpy arrays """
    minr, minc, maxr, maxc = coords
    return image[minr:maxr+1, minc:maxc+1]

def get_cropped_regions(image, coords):
    """ given an image and a bounding box, crop the image, and do specific close-up
        filtering to separate cells. """

    # crop and filter the image
    cropped = crop_image(image, coords)
    cropped = erosion(cropped)
    thresh = threshold_yen(cropped)
    binary_cropped = opening(closing(cropped > thresh, square(3)), square(3))
    labelled_cropped = label(binary_cropped)

    # return the number of regions discovered with an area greater than 200 pixels
    regions = filter(lambda r: r.area >= 200, regionprops(labelled_cropped))
    return list(regions)

def process_image(image, path, fullDirectory = ''):
    """ given an image, process it and return info
        @returns nCells, nSplitting, nWires, nC2CWires, subregions """
    labelled_image = get_labelled_image(image)

    c = 0
    s = 0
    w = 0
    cw = 0
    sr =[]

    cellRegions = []

    for region in regionprops(labelled_image):

        if region.area >= 200 and region.area < 1200:
            minr, minc, maxr, maxc = region.bbox
            cellRegions.append([minr,minc,maxr,maxc])

        if region.area >= 1200:
            regions = get_cropped_regions(image, region.bbox)
            if regions != []:
                for item in regions:
                    bigminr, bigminc, _, _ = region.bbox
                    minr, minc, maxr, maxc = item.bbox
                    '''x = bigminc + minc
                    y = bigminr + minr
                    w = bigminc + maxc
                    l = bigminr + maxr
                    cellRegions.append([y,x,l,w])'''
                    cellRegions.append([minr,minc,maxr,maxc])

            c += len(regions)

            # counts, and the coords
            sr.append({
                "origSubTotal": len(regions),
                "origSubSplitting": 0,
                "origSubCTG": 0,
                "origSubCTC": 0,
                "imgSrc": path,
                "bounds": region.bbox
            })

        elif region.area >= 200:
            c += 1

    w = wirecounter.main(cellRegions, fullDirectory)
    return c, s, w, cw, sr

def write_counts(counts, fn="counts.csv"):
    """ given counts, a list of tuples (filename, count),
        write data to a csv file. """
    with open("processed/" + fn, 'w') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(['filename', 'count'])
        csv_out.writerows(counts)

def main():
    """
    Prompt for user input whether they would like the program to crunch all the
    counts, or if they would like to look at each individual image
    """

    # dir to process: sys.argv[1]
    # nFiles = number of files in dir
    # for i, image in enumerate(dir):
    #   process_image(image)
    #   save overlay image to results folder
    #   append to results/results.csv

    #   send progress status
    #   sendStatus(i / nFiles)

    data_dir = sys.argv[1]

    data = {
        "nProcessed": 0,
        "nToProcess": len(os.listdir(data_dir)),
        "image": "",
        "total": 0,
        "splitting": 0,
        "cellToGround": 0,
        "cellToCell": 0,
    }
    cell_counts = []

    accepted_filetypes = [".JPG", ".PNG", "JPEG"]
    # TODO: update this code block to send the above data, specified for each processed image, to the server,
    # e.g. data["image"] = fn; output(data)
    for i, fn in enumerate(os.listdir(data_dir)):
        if fn[-4:].upper() in accepted_filetypes:
            # get full path and image object
            path = os.path.join(data_dir, fn)
            image = io.imread(path, as_gray=True)

            nCells, nSplit, nWires, nC2CWires, subregions = process_image(image, fn, path)

            # get count for each image
            data.update({
                "image": fn,
                "nProcessed": data['nProcessed'] + 1,
                "nToProcess": data['nToProcess'] - 1,
                "total": nCells,
                "splitting": nSplit,
                "cellToGround": nWires,
                "cellToCell": nC2CWires,
                "rowData": subregions
            })

            if (data):
                # write down count, and send data serverside
                cell_counts.append((fn, data['total']))
                output(json.dumps(data))

    # write to csv file
    # write_counts(cell_counts)

main()
