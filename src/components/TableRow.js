import React, { Component } from 'react';
import { Table, Button } from 'semantic-ui-react';

import '../stylesheets/TableRow.scss';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SubImage from './SubImage.js';
import {
  updateSubTotal,
  updateSubSplitting,
  updateSubCTG,
  updateSubCTC,
  revertRow,
} from '../reducers/subCounts';

class TableRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorTotal: false,
      errorSplittng: false,
      errorCTG: false,
      errorCTC: false,
      subTotal: 0,
      subSplitting: 0,
      subCTG: 0,
      subCTC: 0,
      checkOn: true
    }
  }

  componentWillReceiveProps(nextProps) {
    const { megadata, currImgPage, idx } = nextProps;
    const rowData = megadata[currImgPage].rowData[idx];
    this.setState({
      subTotal: rowData.updatedSubTotal || rowData.origSubTotal,
      subSplitting: rowData.updatedSubSplitting || rowData.origSubSplitting,
      subCTC: rowData.updatedSubCTC || rowData.origSubCTC,
      subCTG: rowData.updatedSubCTG || rowData.origSubCTG,
    })
  }

  handleTotalCellChange = (e) => {
    this.setState({subTotal: e.target.value});
  };

  handleSplittingChange = (e) => {
    this.setState({subSplitting: e.target.value});
  };

  handleCellToGroundChange = (e) => {
    this.setState({subCTG: e.target.value});
  };

  handleCellToCellChange = (e) => {
    this.setState({subCTC: e.target.value});
  };

  revertCounts = () => {
    const {idx, megadata, currImgPage } = this.props;
    const { subTotal, subSplitting, subCTG, subCTC } = this.state;
    const { origSubTotal, origSubSplitting, origSubCTG, origSubCTC } = megadata[currImgPage].rowData[idx];

    this.props.revertRow(subTotal, subSplitting, subCTG, subCTC, idx);
    this.setState({
      subTotal: origSubTotal,
      subSplitting: origSubSplitting,
      subCTG: origSubCTG,
      subCTC: origSubCTC,
      errorTotal: false,
      errorSplittng: false,
      errorCTG: false,
      errorCTC: false
    });
    this.setState({checkOn: true})
  };

  checkInputs = (e) => {
    e.preventDefault();
    const { idx, megadata, currImgPage } = this.props;
    let { subTotal, subSplitting, subCTG, subCTC} = this.state;
    let { origSubTotal, origSubSplitting, origSubCTG, origSubCTC,
          updatedSubTotal, updatedSubSplitting, updatedSubCTC, updatedSubCTG } = megadata[currImgPage].rowData[idx]

    // set error flag that will change styling around input box if user inputs a non-number
    if (isNaN(subTotal)) {
      this.setState({errorTotal: true});
      return;
    }
    if (isNaN(subSplitting)) {
      this.setState({errorSplittng: true});
      return;
    }
    if (isNaN(subCTG)) {
      this.setState({errorCTG: true});
      return;
    }
    if (isNaN(subCTC)) {
      this.setState({errorCTC: true});
      return;
    }

    // update overall results depending on which counts were updated
    let newSubTotal = parseInt(subTotal, 10);
    let newSubSplitting = parseInt(subSplitting, 10);
    let newSubCTG = parseInt(subCTG, 10);
    let newSubCTC = parseInt(subCTC, 10);

    if (subTotal !== origSubTotal && subTotal !== updatedSubTotal) {
      this.setState({subTotal: newSubTotal});
      this.props.updateSubTotal(newSubTotal, idx);
    }
    if (subSplitting !== origSubSplitting && subSplitting !== updatedSubSplitting ) {
      this.setState({subSplitting: newSubSplitting});
      this.props.updateSubSplitting(newSubSplitting, idx);
    }
    if (subCTG !== origSubCTG && subCTG !== updatedSubCTG) {
      this.setState({subCTG: newSubCTG});
      this.props.updateSubCTG(newSubCTG, idx);

    }
    if (subCTC !== origSubCTC && subCTC !== updatedSubCTC) {
      this.setState({subCTC: newSubCTC});
      this.props.updateSubCTC(newSubCTC, idx);
    }
    this.setState({checkOn: false})
  };

  render() {
    const { imgSrc, megadata, currImgPage, idx } = this.props;
    let { subTotal, subSplitting, subCTC, subCTG,
          errorTotal, errorSplittng, errorCTG, errorCTC} = this.state;
    const row = megadata[currImgPage].rowData[idx];
    const { bounds } = row
    return(
      <Table.Row>
        <Table.Cell collapsing>
          <SubImage src={imgSrc} bounds={bounds} />
        </Table.Cell>
        <Table.Cell collapsing>
          <div className="update-cell-text">Total number of cells</div>
          <div className="update-input">
            <input
              className={errorTotal ? "error-input" : ""}
              value={subTotal}
              onChange={this.handleTotalCellChange} />
          </div>
        </Table.Cell>
        <Table.Cell collapsing>
          <div className="update-cell-text">Splitting cells</div>
          <div className="update-input">
            <input
              className={errorSplittng ? "error-input" : ""}
              value={subSplitting}
              onChange={this.handleSplittingChange}/>
          </div>
        </Table.Cell>
        <Table.Cell collapsing>
          <div className="update-cell-text">Cell-to-ground connections</div>
          <div className="update-input">
            <input
              className={errorCTG ? "error-input" : ""}
              value={subCTG}
              onChange={this.handleCellToGroundChange}/>
          </div>
        </Table.Cell>
        <Table.Cell collapsing>
          <div className="update-cell-text">Cell-to-cell connections</div>
          <div className="update-input">
            <input
              className={errorCTC ? "error-input" : ""}
              value={subCTC}
              onChange={this.handleCellToCellChange}/>
          </div>
        </Table.Cell>
        <Table.Cell collapsing>
          <Button onClick={this.revertCounts}>
            <i class="fas fa-undo-alt"></i>
          </Button>
          <button
            className="check-button" onClick={this.checkInputs}
            disabled={!this.state.checkOn}>
            <i className="fas fa-check"></i>
          </button>
        </Table.Cell>
      </Table.Row>
    );
  }
}

const mapStateToProps = ({state}) => ({
  megadata: state.megadata,
  currImgPage: state.currImgPage
});

const mapDispatchToProps = dispatch => bindActionCreators({
  updateSubTotal,
  updateSubSplitting,
  updateSubCTG,
  updateSubCTC,
  revertRow
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TableRow);
