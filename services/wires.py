"""
===================
Label image regions
===================
http://scikit-image.org/docs/dev/auto_examples/segmentation/plot_label.html#sphx-glr-download-auto-examples-segmentation-plot-label-py

This example shows how to segment an image with image labelling. The following
steps are applied:

1. Thresholding with automatic yen method
2. Close small holes with binary closing
3. Measure image regions to filter small objects


TODO:
 investigate regions
 iterate through images
 check with labels

"""

import os, csv, glob
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import cv2 as cv


from skimage import data, io
from skimage.util import img_as_ubyte
from skimage.filters import threshold_yen, threshold_isodata, threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.color import label2rgb, rgb2gray
from PIL import Image

from skimage.morphology import erosion, dilation, opening, closing, white_tophat
from skimage.morphology import black_tophat, skeletonize, convex_hull_image
from skimage.morphology import disk, square

from scipy.misc import imread

###############################################################################
###############################################################################
###############################################################################



class Picture:
    def __init__(self, file):
        self.pic = Image.open(file).convert('L')
        self.img = np.array(self.pic)
        self.pixls = 0
        self.visited = np.zeros((len(self.img), len(self.img[0])))
        self.wires = 0
        self.c2c = 0
        self.celltocell = 0
        self.threshold = 0
        self.i = []
        self.j = []

    def renew(self, array):
        # set image to given array
        self.img = array

    def update(self, i, j, val):
        # set specific location in image to given pixel value
        self.img[i][j] = val

    def ret(self):
        # return array representation of image
        return self.img

    def pixels(self):
        # pixels visited by wiresearch
        return self.pixls

    def total_pixels(self, number):
        # update pixels visited by wiresearch
        self.pixls = number

    def not_visited(self, i, j):
        # tell whether the pixel location has already been visited by search
        return self.visited[i][j] == 0

    def been_visited(self, i, j, x):
        # mark location as visited

        self.visited[i][j] = x

    def at_cell(self, i, j):
        # tells if we have hit a cell
        # needs more thought
        return self.visited[i][j] == 1

    def inc_wire(self):
        #increment wire count
        self.wires += 1

    def num_wires(self):
        # return the number of wires
        return self.wires

    def act_c2c(self, x):
        #update number of pixels touching another cell
        self.c2c = x

    def num_c2c(self):
        #return number of cell to cell connections
        return self.c2c

    def inc_c2c(self):
        # increment cell to cell connetions
        self.celltocell +=1

    def connections(self):
        # return number of cell to cell connections
        return self.celltocell

    def set_t(self, x):
        # set threshold for pooling image
        self.threshold = x

    def get_t(self):
        # get threshold for pooling image
        return self.threshold



def inbounds(img, i, j, dx = 1, dy = 1):
    return (i >= 0) and (i < len(img)) and (j >= 0) and (j < len(img[0])) and not((dx==0) and (dy == 0))



'''def adjacency(img, x, y, queue= []):
    queue.append((x, y))
    count = 0
    while queue:
        x, y = queue.pop()
        if inbounds(img.ret(), x, y) and (img.ret()[x][y] == 255) and (img.not_visited(x, y)):
            count += 1
            img.update(x, y, 100)
            img.been_visited(x, y, 2)
            img.total_pixels((img.pixels()+1))
            for i in range(-1,2):
                for j in range(-1,2):
                    if inbounds(img.ret(), x+i, y+j,i,j) and (img.ret()[x+i][y+j] == 255) and (img.not_visited(x+i, y+j)):
                        queue.append((x+i,y+j))


    return count
'''
def adjacency(img, x, y, queue= []):
    queue.append((x, y))
    count = 0
    while queue:
        x, y = queue.pop()
        if inbounds(img.ret(), x, y) and (img.ret()[x][y] == 255) and (img.not_visited(x, y)):
            count += 1
            img.i.append(x)
            img.j.append(y)
            #img.update(x,y,100)
            img.been_visited(x, y, 2)
            img.total_pixels((img.pixels()+1))
            queue.append((x-1, y-1))
            queue.append((x-1, y))
            queue.append((x-1, y+1))
            queue.append((x, y-1))
            queue.append((x, y+1))
            queue.append((x+1, y-1))
            queue.append((x+1, y))
            queue.append((x+1, y+1))
    return count

def adj(img, i, j):
    img.i = []
    img.j = []
    img.total_pixels(0)
    img.act_c2c(0)
    if inbounds(img.ret(), i, j) and img.not_visited(i, j) and img.ret()[i][j] == 255:
        adjacency(img, i, j)
    if img.pixels() > 90:
        img.inc_wire()
        for i in range(0,len(img.i)):
            img.update(img.i[i],img.j[i],100)
    if img.num_c2c() > 1:
        img.inc_c2c()
    return
    #print(img.num_wires())


def p2(img):
    newi=img.ret().copy()
    filter = [[0,0,0],[0,0,0],[0,0,0]]
    for i in range(1,len(img.ret())-1):
        for j in range(1,len(img.ret()[0])-1):
            sum = 0
            for x in range(-1,2):
                for z in range(-1,2):
                    filter[x+1][z+1]=img.ret()[i+x][j+z]
                    sum += img.ret()[i+x][j+z]
            if (sum/9) > img.get_t():
                newi[i, j] = 255
            else:
                newi[i, j] = 0
    img.renew(newi)
    return


def perctl(img):
    dist = np.zeros(256, dtype = int)

    for i in range(0,len(img.ret())):
        for j in range(0,len(img.ret()[0])):
            dist[img.ret()[i][j]] +=1
    tot = 0
    x=len(img.ret())*len(img.ret()[0])
    for i in range(0,len(dist)):
        tot += dist[i]
        if (tot/x)*100 >= 96:
            img.set_t(i)
            return

def write_counts2(values):
    with open("tables/machined_counts.csv", 'w') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(['filename', 'count'])
        for i in range(0, len(values)):
            csv_out.writerow([values[i][0], values[i][1]])

def plot_comparison(original, filtered, filter_name):
    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(8, 4), sharex=True,
                                   sharey=True)
    ax1.imshow(original, cmap=plt.cm.gray)
    ax1.set_title('original')
    ax1.axis('off')
    ax2.imshow(filtered, cmap=plt.cm.gray)
    ax2.set_title(filter_name)
    ax2.axis('off')
    plt.show()


def boundbox(img, regions):

    for item in regions:
    #print(item)
        for i in range(item[0],item[2]):
            for j in range(item[1],item[3]):
                #img.update(i, j, 200)
                img.been_visited(i, j, 1)
    return


def wiresearch(img, regions):
    for item in regions:

        for i in range(item[0]-1,item[2]+1):

            if inbounds(img.ret(), i, item[1]-1):
                #img.update(i, item[1]-1, 255)
                adj(img, i, item[1]-1)
            if inbounds(img.ret(), i, item[3]):
                #img.update(i, item[3], 255)
                adj(img, i, item[3])


        for i in range(item[1],item[3]+1):

            if inbounds(img.ret(), item[0]-1, i):
                adj(img, item[0]-1, i)
                #img.update(item[0]-1, i, 255)
            if inbounds(img.ret(), item[0], i):
                adj(img, item[2], i)
                #img.update(item[2], i, 255)

    return

def main(regions, filename = ""):
    if filename == "":
        return -1

    values = []

    # for filename in glob.glob('./original/*.jpg'):
    # for filename in glob.glob('./*.jpg'):


    # label = filename.split('/')[2]
    # print(label)
    # print(' ')
    # print(filename)

    img = Picture(filename)


    # regions = label_regions(img.ret())
    #minr, minc, maxr, maxc
    boundbox(img, regions)

    img.renew(np.array(Image.fromarray(cv.fastNlMeansDenoisingColored(cv.cvtColor(img.img, cv.COLOR_GRAY2BGR) ,None,10,10,7,21)).convert('L')))

    perctl(img)
    # print(img.get_t())
    p2(img)

    # print('p2 done')



    wiresearch(img, regions)
    rgb = cv.cvtColor(img.ret(),cv.COLOR_GRAY2RGB)
    for i in range(0,len(img.ret())):
        for j in range(0,len(img.ret()[0])):
            if img.ret()[i][j] >50 and img.ret()[i][j] <150:
                rgb[i][j] = [200,0,0]

    # path = "./finalImage.jpg"
    # rgb = Image.fromarray(rgb)
    # rgb.save(path)

    # print('wires =', img.num_wires())
    # values.append((label, img.num_wires()))

    return img.num_wires()

    # write_counts2(values)

if __name__ == "__main__":
    main()
