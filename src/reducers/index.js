import { combineReducers } from 'redux';
import subCounts from './subCounts';

export default combineReducers({
  state: subCounts
})
