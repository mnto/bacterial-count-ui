import React, { Component } from 'react';
import { Image, Table, Button } from 'semantic-ui-react';
import TableRow from './TableRow.js';
import { connect } from 'react-redux';
import path from 'path';
import { bindActionCreators } from 'redux';
import {
  addImgData,
  revertAll
} from '../reducers/subCounts';

import '../stylesheets/ImageProcessView.scss';

class ImageProcessView extends Component {
  constructor() {
    super();
    this.state = {
      sticky: false
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll.bind(this));
  }

  handleScroll = () => {
    if (window.pageYOffset > 300) {
      this.setState({sticky: true})
    } else {
      this.setState({sticky: false})
    }
  };

  revertAllChanges = () => {
    this.props.revertAll();
  }

  render() {
    const { megadata, currImgPage } = this.props;
    const imgData = megadata[currImgPage];

    const { image, total, splitting, cellToGround, cellToCell, rowData,
            updatedTotal, updatedSplitting, updatedCellToGround, updatedCellToCell } = imgData
    return (
      <div className="img-process-view">
        <div className={this.state.sticky ? "orig-img-info-sticky" : "orig-img-info" }>
          <Image src={path.join('/uploads', 'latest', image)} className={this.state.sticky ? "min-orig-img": "orig-img"} />
          <Table className="orig-img-data" celled basic="very">
            <Table.Body>
              <Table.Row>
                <Table.Cell>Total cells</Table.Cell>
                <Table.Cell>{updatedTotal > 0 ? updatedTotal : total }</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Splitting cells</Table.Cell>
                <Table.Cell>{updatedSplitting > 0? updatedSplitting : splitting}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Cell-to-ground connections</Table.Cell>
                <Table.Cell>{updatedCellToGround > 0? updatedCellToGround : cellToGround}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Cell-to-cell connections</Table.Cell>
                <Table.Cell>{updatedCellToCell > 0 ? updatedCellToCell : cellToCell}</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </div>

        <div className="update-info-section">
          <p className="header-text">Help us improve the outputs</p>
          <Table className="edit-output-table" celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan='6'>Identified Problematic Cell Clusters</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {
                rowData.map((row, index) =>
                  <TableRow
                    key={index}
                    idx={index}
                    imgSrc={path.join(__dirname, '/uploads', 'latest', image)}
                  />
                )
              }
            </Table.Body>
          </Table>
          <Button onClick={this.revertAllChanges}>Undo all changes</Button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({state}) => ({
  megadata: state.megadata,
  currImgPage: state.currImgPage
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addImgData,
  revertAll
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ImageProcessView);
