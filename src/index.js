import React from 'react';
import { render } from 'react-dom';
import './index.scss';
import App from './App';
import 'semantic-ui-css/semantic.min.css';
import store from './store'
import { Provider } from 'react-redux';

const target = document.querySelector('#root');

render(
  <Provider store={store}>
      <div>
        <App />
      </div>
  </Provider>,
  target
)
