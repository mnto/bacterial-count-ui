# Bacterial Count UI

This is a user interface (React App) for the Bacterial Count App, with the backend written in Python

CS Senior Thesis 2018-2019. Hamilton College Computer Science Department

## To run the project
1. `git clone git@gitlab.com:mnto/bacterial-count-ui.git`
2. `yarn install` or `npm install`
3. `yarn run start` or `npm run start` to run both the frontend and backend at the same time
4. The page should be up and running at `localhost:8080`.
