import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isEmpty } from 'lodash';
import { Progress, Loader } from 'semantic-ui-react';

import {
  addImgData,
  setCurrImgPage,
} from '../reducers/subCounts';

import ImageProcessView from './ImageProcessView';
import PaginationNavigation from './PaginationNavigation'
import DownloadButton from './DownloadButton';
import ScrollButton from './ScrollButton';

import '../stylesheets/ProcessView.scss';

class ProcessView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
      nImages: 0,
      page: 0,
      data: []
    };
  }

  componentDidMount() {
    /**
     * server update event that occurs when a new image is processed
     * COUNT types are cell, wire, splitting cell, and cell-to-cell wire.
     *
     * process view should update according to its state, using the following properties:
     * state = {
     *  progress: percentage of images processed so far
     *  counts: list of count objects, one for each image which contains each of the COUNT types
     *    example count = { filename, cell_count, splitting_count, wire_count, cc_wire_count }
     *  rows: list of row objects, one for each subregion of uncertainty
     *    example row = { index, filename, coordinates, cell_count, splitting_count, wire_count, cc_wire_count }
     * }
     *
     * state.counts will be used to output the final counts in csv format.  written as a list of dictionaries.
     *
     * state.rows will be used to populate the rows at the bottom of the page,
     *            which are used to update counts in regions the script couldn't process, i.e. 'regions of uncertainty'
     *
     * @param data = {
     *          nProcessed: the number of images processed so far
     *          nToProcess: the number of images to be processed still
     *          image_fn: image filename
     *          nCells: cell count
     *          nSplit: splitting cell count
     *          nWires: wire count (to ground)
     *          nC2CWires: cell to cell count
     *          subregions: array of coordinates defining subregions of uncertainty
     *        }
     */
    this.props.socket.on('process-update', (imgData) =>
    {
      console.log('got process update!', imgData);
      const { nProcessed, nToProcess, image, total, splitting, cellToCell, cellToGround, rowData } = imgData

      // generate the new row for counts
      const newImgData = {
                        image,
                        total,
                        splitting,
                        cellToGround,
                        cellToCell,
                        rowData
                      }
      const percent = nProcessed / (nProcessed + nToProcess)

      // update progress and counts
      this.setState((prev) => ({
        buffer: Math.floor(percent * 100),
        data: prev.data.concat([newImgData])
      }))

      // if statement somewhat redundant but it's there to make sure we only change this on the initial object
      if (this.state.nImages <= nToProcess) {
      console.log("setting nImages to: " + nToProcess + "+" + nProcessed)
        this.setState({
          nImages: nToProcess + nProcessed
        })
      }

      this.props.addImgData(this.state.data[this.state.data.length - 1]);
      this.props.setCurrImgPage(this.state.page);
    })

    this.render()
  }

  // sets state.page to index
  setPage(index) {
    if (index < this.state.data.length) {
      this.props.setCurrImgPage(index);
    }
  }

  render() {
    const { nImages, buffer } = this.state;
    const { megadata, currImgPage } = this.props;
    const currImg = megadata[currImgPage];
    return (
      <div className="page-view">
        <div className="content-container">
          <p className="header-text">Processing image files...</p>
          <Progress className="progress-bar" percent={buffer} progress color='green' >{buffer} % data back from analysis</Progress>
          {isEmpty(currImg) ?
            <Loader style={{'margin': '30px auto'}}active inline='centered'></Loader> :
            <div>
              <DownloadButton socket={this.props.socket} />
              <PaginationNavigation style={{marginTop: "2em"}}
                                    forcePage={this.state.page}
                                    pages={nImages}
                                    setPage={(page) => this.setPage(page)}
                                    hidden={this.state.nToProcess === 0}
                                    />
              <ImageProcessView />
            </div>
          }
          <ScrollButton />
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ state }) => ({
  megadata: state.megadata,
  currImgPage: state.currImgPage
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addImgData,
  setCurrImgPage,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ProcessView);
