import React, { Component } from 'react';
import '../stylesheets/ScrollButton.scss';
import { Button, Icon } from 'semantic-ui-react';

export default class ScrollButton extends Component {
  scrollToTop = () => {
    window.scrollTo({
      top: 300,
      behavior: "smooth"
    })
  }

  render () {
      return <Button title='Back to top'
                     className='scroll'
                     onClick={this.scrollToTop}>
                <Icon className='arrow-up' name='angle double up' size='large'></Icon>
             </Button>;
  }
};
