/**
 * server file for the bacterial count ui.
 * serves our react frontend, and
 * allows uploading image files through a POST to /upload, storing them locally
 * and triggers our python script to process the uploaded images
 * implements socket events to transmit information and files.
 * re: files - use sockets for images instead of POST?
 */

var fs = require('fs')
var lnf = require('lnf')
var path = require('path')
var serveStatic = require('serve-static')
var spawn = require("child_process").spawn;

var express = require('express')
var app = express()

//var key = fs.readFileSync( '/etc/pki/tls/private/localhost.key' );
//var cert = fs.readFileSync( '/etc/pki/tls/certs/localhost.crt' );

var port = 80
//var https = require('https').createServer({key, cert}, app);
var http = require('http').createServer(app);

var server = http
if (port == 80) {
    console.log("Using insecure http server...")
    server = http
}

var io = require('socket.io')(server)

/**
 * returns a string timestamp for the directory creation
 * format: YYMMDD HHMMSS
 * example: 181019 205345
 * TODO: add zero padding on each part
 */
function getTimestamp() {
  const now = new Date()
  const YY = now.getYear() - 100
  const MM = now.getMonth() + 1
  const DD = now.getDate()
  const HH = now.getHours()
  const mm = now.getMinutes()
  const SS = now.getSeconds()
  return "" + YY + MM + DD + " " + HH + mm + SS
}

/**
 * creates the directory to place these images in
 */
async function createDir() {
  // generate a folder path, e.g. /uploads/181019 20350
  const uploads = path.join(__dirname, "build", "uploads")
  const uploadFolder = path.join(uploads, getTimestamp())

  // create folder to save images to
  if (! fs.existsSync(uploads))
    fs.mkdirSync(uploads)
  fs.mkdirSync(uploadFolder)

  // move latest symbolic link to the new uploadFolder by
  // unlinking it if it exists, and relinking it to the new folder
  const latestLink = path.join(uploads, 'latest')

  lnf(uploadFolder, latestLink)

  return uploadFolder
}

/**
 * save files to disk
 */
async function saveFiles(uploadFolder, files, socket) {
  let successCount = 0;

  console.log('received images-sent event, saving ' +
              files.images.length + ' files to ' + uploadFolder)

  // iterate through the images
  for (var i = 0; i < files.images.length; i++) {
    // generate a filepath for each image
    // console.log("uploadfolder:" + uploadFolder)
    // console.log("files.names[i]:" + files.names[i])
    const imagepath = path.join(uploadFolder, files.names[i])
    // console.log("writing images " + successCount + " to " + imagepath)

    // write the image to file
    fs.writeFileSync(imagepath, files.images[i], 'base64')
    // console.log("Successfully saved image " + successCount + " to " + imagepath)
    successCount++
  }

  console.log("Successfully saved " + successCount + " images.")
}

/**
 * process the files in the /uploads/latest/ directory,
 * and place processed files in a mirrored directory in /processed/
 */
async function processFiles(socket) {
  // call python script with directory argument
  // https://stackoverflow.com/a/23452742/3972042
  console.log('calling python process')
  const process = spawn('python3', ['-u',
                                    'services/process_image_dir.py',
                                    './build/uploads/latest'])
  console.log('waiting for python response...')

  // debug errors from the python script
  process.stderr.on('error', (error) => {
    console.error("PYTHON ERROR!")
    console.error(error)
  })

  // since data is chunked in buffer, need to concatenate them to get full json
  process.stdout.on('data', (data) => {
    console.log("recieved json data from py script, parsing and emitting via socket - ");
    if (data !== undefined) {
      try {
        socket.emit('process-update', JSON.parse(data));
      } catch(e) {
        console.error("Error parsing python data.")
        console.error(e)
      }
    }
  });
}

async function getDownloadableFiles(megadata) {
  console.log('writing to csv:');
  let csvStr = "Image,Total Cells,Splitting Cells,Cell to Cell,Cell to Ground\r\n";
  try {
    console.log(megadata);
    megadata.forEach(imgData => {
      var line = '';
      for (var key in imgData) {
        if (key ==="image") {
          if (line !== '') {
            line += ','
          }
          line += imgData[key];
        }
        else if (key === "total") {
          line += ','
          line += "updatedTotal" in imgData ? imgData.updatedTotal : imgData.total;
          // console.log('Total:', imgData.updatedTotal, imgData.total);

        }
        else if (key === "splitting") {
          line += ','
          line += "updatedSplitting" in imgData ? imgData.updatedSplitting : imgData.splitting;
          // console.log('splitting', imgData.updatedSplitting, imgData.splitting);
        }
        else if (key === "cellToCell") {
          // console.log('cellToCell');
          line += ','
          line += "updatedCellToCell" in imgData ? imgData.updatedCellToCell : imgData.cellToCell;
        }
        else if (key === "cellToGround") {
          // console.log('cellToGround');
          line += ','
          line += "updatedCellToGround" in imgData ? imgData.updatedCellToGround : imgData.cellToGround;
        }
      }
      csvStr += line + '\r\n';
    })
    console.log(csvStr);

    // create downloads folder and put csv there
    const downloadFolder = path.join(__dirname, "build", "downloads");
    if (!fs.existsSync(downloadFolder)) {
      // const labelledImgPath = path.join(downloadFolder, "labelled.jpg")
      // const clusterPath = path.join(downloadFolder)
      fs.mkdirSync(downloadFolder)
    }
    const downloadFilePath = path.join(downloadFolder, "counts.csv");
    fs.writeFileSync(downloadFilePath, csvStr);
  } catch(err) {
    console.log('Error downloading data', err);
  }
}

// host the build directory for GET requests
app.use(serveStatic(path.join(__dirname, '/build')))

// instantiate socket connection
io.on('connection', (socket) => 
{
  // when the frontend uploads images, save them and then process them.
  // both functions called emit socket events to inform the front end of results
  socket.on('send-images-begin', async () => 
  {
    console.log('send images began')
    const dir = await createDir()
    socket.on('send-images', async (files) => 
    {
      // given a list of files, save those to a folder
      await saveFiles(dir, files, socket)
    })
    socket.on('send-images-end', async () => 
    {
      // trigger processing, which uses the socket to 
      // transfer data back to the frontend
      console.log("send images ended")
      await processFiles(socket)
      socket.emit('images-saved')
    })
  })

  // listen for data saving and let frontend know when csv file is created
  socket.on('save-data', async (megadata) => {
    await getDownloadableFiles(megadata);
    socket.emit('data-saved');
  })
})

// open port 80 for http requests, to avoid sudo
server.listen(port, () => {
  console.log("listening on port " + port + "...")
})
