import { isEmpty } from 'lodash';

export const SET_MEGA_DATA = 'store/SET_MEGA_DATA'
export const SET_CURR_PAGE = 'store/SET_CURR_PAGE'
export const UPDATE_SUB_TOTAL = 'store/UPDATE_SUB_TOTAL'
export const UPDATE_SUB_SPLITTING = 'store/UPDATE_SUB_SPLITTING'
export const UPDATE_SUB_CTC = 'store/UPDATE_SUB_CTC'
export const UPDATE_SUB_CTG = 'store/UPDATE_SUB_CTG'
export const REVERT_ROW = 'store/REVERT_ROW'
export const REVERT_ALL = 'stotre/REVERT_ALL'

const initialState = {
  megadata: [],
  currImgPage: 0
}

// reducers
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_MEGA_DATA:
      let newMegadata = JSON.parse(JSON.stringify(state.megadata))
      if (isEmpty(newMegadata)) {
        newMegadata = [action.imgData];
      } else {
        newMegadata.push(action.imgData);
      }

      return {
        ...state,
        megadata: newMegadata
      }

    case SET_CURR_PAGE:
      return {
        ...state,
        currImgPage: action.page
      }

    case UPDATE_SUB_TOTAL:
      const page = state.currImgPage;
      const idx = action.idx;
      let updatedSubTotal = action.updatedSubTotal;

      // isolate the row that needs to be updated
      let newData = JSON.parse(JSON.stringify(state.megadata))

      // create new fields for updated counts and calculates the new counts
      newData[page].rowData[idx] = {
        ...state.megadata[page].rowData[idx],
        updatedSubTotal
      };

      newData[page] = {
        ...newData[page],
        updatedTotal: state.megadata[page].updatedTotal ?
          state.megadata[page].updatedTotal + updatedSubTotal - state.megadata[page].rowData[idx].origSubTotal :
          state.megadata[page].total + updatedSubTotal - state.megadata[page].rowData[idx].origSubTotal
      }

      return {
        ...state,
        megadata: newData,
      };

    case UPDATE_SUB_SPLITTING:
      const pageSubSplitting = state.currImgPage
      const subSplittingIdx = action.idx
      let updatedSubSplitting = action.updatedSubSplitting;
      let subSplittingNewData = JSON.parse(JSON.stringify(state.megadata));

      // create new fields for updated counts and calculates the new counts
      subSplittingNewData[pageSubSplitting].rowData[subSplittingIdx] = {
        ...state.megadata[pageSubSplitting].rowData[subSplittingIdx],
        updatedSubSplitting
      };

      subSplittingNewData[pageSubSplitting] = {
        ...subSplittingNewData[pageSubSplitting],
        updatedSplitting: state.megadata[pageSubSplitting].updatedSplitting ?
          state.megadata[pageSubSplitting].updatedSplitting + updatedSubSplitting - state.megadata[pageSubSplitting].rowData[subSplittingIdx].origSubSplitting :
          state.megadata[pageSubSplitting].splitting + updatedSubSplitting - state.megadata[pageSubSplitting].rowData[subSplittingIdx].origSubSplitting
      }

      return {
        ...state,
        megadata: subSplittingNewData,
      };

    case UPDATE_SUB_CTC:
      const pageSubCTC = state.currImgPage
      const subCTCInd = action.idx
      let updatedSubCTC = action.updatedSubCTC;
      let subCTCNewData = JSON.parse(JSON.stringify(state.megadata));

      // create new fields for updated counts and calculates the new counts
      subCTCNewData[pageSubCTC].rowData[subCTCInd] = {
        ...state.megadata[pageSubCTC].rowData[subCTCInd],
        updatedSubCTC
      };

      subCTCNewData[pageSubCTC] = {
        ...subCTCNewData[pageSubCTC],
        updatedCellToCell: state.megadata[pageSubCTC].updatedCellToCell ?
          state.megadata[pageSubCTC].updatedCellToCell + updatedSubCTC - state.megadata[pageSubCTC].rowData[subCTCInd].origSubCTC :
          state.megadata[pageSubCTC].cellToCell + updatedSubCTC - state.megadata[pageSubCTC].rowData[subCTCInd].origSubCTC,
      }

      return {
        ...state,
        megadata: subCTCNewData,
      };

    case UPDATE_SUB_CTG:
      const pageSubCTG = state.currImgPage
      const subCTGInd = action.idx
      let updatedSubCTG = action.updatedSubCTG;
      let subCTGNewData = JSON.parse(JSON.stringify(state.megadata));

      // create new fields for updated counts and calculates the new counts
      subCTGNewData[pageSubCTG].rowData[subCTGInd] = Object.assign(subCTGNewData[pageSubCTG].rowData[subCTGInd], {
        updatedSubCTG
      });

      subCTGNewData[pageSubCTG] = {
        ...subCTGNewData[pageSubCTG],
        updatedCellToGround: state.megadata[pageSubCTG].updatedCellToGround ?
          state.megadata[pageSubCTG].updatedCellToGround + updatedSubCTG - state.megadata[pageSubCTG].rowData[subCTGInd].origSubCTG :
          state.megadata[pageSubCTG].cellToGround + updatedSubCTG - state.megadata[pageSubCTG].rowData[subCTGInd].origSubCTG,
      }

      return {
        ...state,
        megadata: subCTGNewData,
      };

    case REVERT_ROW:
      const pageNumber = state.currImgPage;
      const ix = action.ix;
      const subTotal = action.currSubTotal;
      const subSplitting = action.currSubSplitting;
      const subCTC = action.currSubCTC;
      const subCTG = action.currSubCTG;
      let origData = JSON.parse(JSON.stringify(state.megadata));

      const updatedTotal = state.megadata[pageNumber].updatedTotal ?
        state.megadata[pageNumber].updatedTotal - subTotal + state.megadata[pageNumber].rowData[ix].origSubTotal :
        undefined

      const updatedSplitting = state.megadata[pageNumber].updatedSplitting ?
        state.megadata[pageNumber].updatedSplitting - subSplitting + state.megadata[pageNumber].rowData[ix].origSubSplitting :
        undefined

      const updatedCellToCell = state.megadata[pageNumber].updatedCellToCell ?
        state.megadata[pageNumber].updatedCellToCell - subCTC + state.megadata[pageNumber].rowData[ix].origSubCTC :
        undefined

      const updatedCellToGround = state.megadata[pageNumber].updatedCellToGround ?
        state.megadata[pageNumber].updatedCellToGround - subCTG + state.megadata[pageNumber].rowData[ix].origSubCTG :
        undefined

      origData[pageNumber] = Object.assign({}, state.megadata[pageNumber], {
         updatedTotal,
         updatedSplitting,
         updatedCellToCell,
         updatedCellToGround
      })

      delete origData[pageNumber].rowData[ix].updatedSubTotal;
      delete origData[pageNumber].rowData[ix].updatedSubSplitting;
      delete origData[pageNumber].rowData[ix].updatedSubCTC;
      delete origData[pageNumber].rowData[ix].updatedSubCTG;

      if (origData[pageNumber].updatedTotal === state.megadata[pageNumber].total) {
        delete origData[pageNumber].updatedTotal;
      }
      if (origData[pageNumber].updatedSplitting === state.megadata[pageNumber].splitting) {
        delete origData[pageNumber].updatedSplitting;
      }
      if (origData[pageNumber].updatedCellToCell === state.megadata[pageNumber].cellToCell) {
        delete origData[pageNumber].updatedCellToCell;
      }
      if (origData[pageNumber].updatedCellToGround === state.megadata[pageNumber].cellToGround) {
        delete origData[pageNumber].updatedCellToGround;
      }

      return {
        ...state,
        megadata: origData
      }

    case REVERT_ALL:
      let copyData = JSON.parse(JSON.stringify(state.megadata));
      const pageNum = state.currImgPage;

      copyData[pageNum] = {
        ...state.megadata[pageNum],
        updatedTotal: undefined,
        updatedSplitting: undefined,
        updatedCellToCell: undefined,
        updatedCellToGround: undefined
      }

      // set all updates to null
      copyData[pageNum].rowData.forEach(row => {
        row.updatedSubTotal = undefined;
        row.updatedSubSplitting = undefined;
        row.updatedSubCTC = undefined;
        row.updatedSubCTG = undefined;
      })

      return {
        ...state,
        megadata: copyData
      }

    default:
      return state
  }
}

/* dispatch actions */

// sets store imgData
export const addImgData = (imgData) => {
  return dispatch => {
    return dispatch({
      type: SET_MEGA_DATA,
      imgData
    })
  }
};

export const setCurrImgPage = (page) => {
  return dispatch => {
    return dispatch({
      type: SET_CURR_PAGE,
      page
    })
  }
}

// updates store imgData when one of the outputs changes
export const updateSubTotal = (updatedSubTotal, idx) => {
  return dispatch => {
    dispatch({
      type: UPDATE_SUB_TOTAL,
      updatedSubTotal,
      idx
    })
  }
}

export const updateSubSplitting = (updatedSubSplitting, idx) => {
  return dispatch => {
    dispatch({
      type: UPDATE_SUB_SPLITTING,
      updatedSubSplitting,
      idx
    })
  }
}

export const updateSubCTC = (updatedSubCTC, idx) => {
  return dispatch => {
    dispatch({
      type: UPDATE_SUB_CTC,
      updatedSubCTC,
      idx
    })
  }
}

export const updateSubCTG = (updatedSubCTG, idx) => {
  return dispatch => {
    dispatch({
      type: UPDATE_SUB_CTG,
      updatedSubCTG,
      idx
    })
  }
}

// undo each row
export const revertRow = (currSubTotal, currSubSplitting, currSubCTG, currSubCTC, ix) => {
  return dispatch => {
    dispatch({
      type: REVERT_ROW,
      currSubTotal,
      currSubSplitting,
      currSubCTG,
      currSubCTC,
      ix
    })
  }
}

// undo all changes
export const revertAll = () => {
  return dispatch => {
    dispatch({
      type: REVERT_ALL,
    })
  }
}
