import React, { Component } from 'react';
import ResultFileItem from './ResultFileItem';
import '../stylesheets/ResultsFileList.scss';

/**
 * displays the list of downloadable files, with select buttons
 * @props files string[] a list of filenames
 * @props selectedMask bool[] a mask determining which files are selected
 */
class ResultFileList extends Component {
  state = {
    files: this.props.files,
    selectedMask: this.props.selectedMask
  }

  /**
   * method that runs when the props update
   * checks for differences and appropriately updates state
   * @param {*} nextProps
   */
  componentWillReceiveProps(nextProps) {
    if(nextProps.selectedMask !== this.state.selectedMask) {
      this.setState({selectedMask: nextProps.selectedMask});
    }
  }

  selectFile(i, props) {
    const newMask = props.selectedMask.slice()
    newMask[i] = ! props.selectedMask[i]
    this.props.updateMask(newMask)
  }

  render() {
    return (
      <div className="file-list">
        {this.props.files.map((file, i) =>
          <ResultFileItem
            filename={file}
            checked={this.state.selectedMask[i]}
            handleCheck={() => {this.selectFile(i, this.props)}}
          />)
        }
      </div>
    )
  }
}

export default ResultFileList;
