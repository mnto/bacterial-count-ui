import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Button } from 'semantic-ui-react';
import '../stylesheets/DownloadButton.scss';

class DownloadButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }
  submit = () => {
    const { megadata, socket } = this.props;
    console.log('megadata', megadata);
    socket.emit('save-data', megadata);
    this.setState({ isLoading: true });
    socket.on('data-saved', () => {
      console.log('data saved');
      this.setState({isLoading: false});
      window.location = "/downloads/counts.csv"
    });
  }

  render() {
    return(
      <div style={{'margin': '10px', 'display': 'flex', 'justifyContent': 'flex-end', 'padding': '0 15%'}}>
        <Button
          className="submit-button"
          onClick={this.submit}
          loading={this.state.isLoading}
          disabled={this.state.isLoading}>
          Download (.csv)
        </Button>
      </div>
    );
  }
}

const mapStateToProps = ({ state }) => ({
  megadata: state.megadata,
  currImgPage: state.currImgPage
});

const mapDispatchToProps = dispatch => bindActionCreators({
  //
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DownloadButton);
