/**
 * The 'main method' of our app's UI.  There are three views, each located in their own file imported below.
 * The user is walked through each view serially, with information and interaction options present throughout.
 */
import React, { Component } from 'react';
import io from 'socket.io-client';

import Header from './components/Header.js';
import Footer from './components/Footer.js';

import Uploader from './components/Uploader.js';
import ProcessView from './components/ProcessView.js';

import './stylesheets/main.scss';


// Content.state.view enums. used to determine which view to display in Content
const UPLOADER = 0
const PROCESSOR = 1

class Content extends Component {
  state = {
    view: UPLOADER
  }

  constructor(props) {
    super(props)

    // when the backend tells us the images are processed,
    this.props.socket.on('images-processed', () => {
      // move the content state to the ProcessView
      this.setState({
        view: PROCESSOR
      })
    })

    // move to processing view once images have been saved
    this.props.socket.on('images-saved', () => { this.setState({ view: PROCESSOR }) })
  }

  // depending on the Content's view state, display the correct component
  render() {
    let mainElement = <div className='error'>Error: Content State View has no View</div>
    if (this.state.view === UPLOADER) {
      mainElement = <Uploader socket={this.props.socket} />
    }

    else if (this.state.view === PROCESSOR) {
      mainElement = <ProcessView
                      socket={this.props.socket}
                      />
    }

    return (
      <div className='content'>
        { mainElement }
      </div>
    )
  }
}

/**
 * main app. creates the socket connection and
 * passes it through to the content.
 */
class App extends Component {
  state = {
    socket: io()
  }
  render() {
    return (
      <div className="app">
        <Header/>
        <Content socket={this.state.socket} />
        <Footer/>
      </div>
    );
  }
}

export default App;
