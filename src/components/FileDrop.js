import React, { Component } from 'react';

import Dropzone from 'react-dropzone';
import { Button, Icon, List, Loader} from 'semantic-ui-react';
import { Preload } from 'react-preload';

import '../stylesheets/FileDrop.scss';

/**
 * https://react-dropzone.netlify.com/#!/Styling%20Dropzone
 * the component that allows the user to drag and drop files
 * renders the UI of the file uploader, and keeps track of
 * accepted and rejected files.  On submit, calls the uploadFunc prop
 * to pass the accepted images to the Uploader to POST to the server.
 */
class FileDrop extends Component {
  constructor() {
    super()
    this.state = {
      uploading: false,
      loadingEl: <Loader active inline='centered'></Loader>,
      accepted: [],
      rejected: []
    }
  }

  render() {
    return (
      <div className="drop-container">
        <div className="dropzone">
          <Dropzone
            className="dropbox"
            accept="image/jpeg, image/png"
            onDrop={(accepted, rejected) => { this.setState({ accepted, rejected }); }}
          >
            <div>
              <Icon className="upload-icon" name='upload' size='big' />
              <p>Drag & drop images here, or click to select files.</p>
              <p style={{'fontSize': 15}}>*Only .jpg, .jpeg, .png images will be accepted</p>
            </div>
          </Dropzone>
        </div>
        <Preload
            loadingIndicator={<div>Loading...</div>}
            images={[this.state.loadingImg]}
            autoResolveDelay={3000}
            onError={this._handleImageLoadError}
            onSuccess={this._handleImageLoadSuccess}
            resolveOnError={true}
            mountChildren={true}
        >
          {/* Only render Submit button and list of accepted/rejected files when files have been dropped for simplicity */}
          {this.state.accepted.length > 0 ?
            <div>
              <Button className="upload-button"
                      onClick={() =>
                      {
                        this.setState({uploading: true})
                        this.props.uploadFunc(this.state.accepted)
                      }}>
                {/* make the submit button spin a loading icon when we click it */}
                { this.state.uploading ? this.state.loadingEl
                                       : "Submit for processing" }
              </Button>
              <List divided relaxed className="file-scroll">
                {
                  this.state.accepted.map((acceptedFile, idx) =>
                    <List.Item key={idx} className="file-row">
                      <div className="file-info">
                        <p style={{'fontWeight': 'bold'}}>{acceptedFile.name}</p>
                        <p>{Math.round((acceptedFile.size/1024) * 10) / 10} KB</p>
                      </div>
                      <Icon name="check circle outline" className="accepted-file-icon" size="large"/>
                    </List.Item>
                  )
                }
                {
                  this.state.rejected.map((reject,idx) =>
                    <List.Item key={idx} className="file-row">
                      <div className="file-info">
                        <p style={{'fontWeight': 'bold'}}>{reject.name}</p>
                        <p>{Math.round((reject.size/1024) * 10 / 10)} KB</p>
                      </div>
                      <Icon name="ban" className="rejected-file-icon" size="large"/>
                    </List.Item>
                  )
                }
              </List>
            </div>
            :
            null
          }
        </Preload>

        {/* load the loading image so we don't have to when it appears later */}
        <div style={{display: "none"}}>
          { this.state.loadingImg }
        </div>
      </div>
    );
  }
}

export default FileDrop;
