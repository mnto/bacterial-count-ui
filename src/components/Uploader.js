import React, { Component } from 'react';
import FileDrop from './FileDrop';

import '../stylesheets/Uploader.scss';


/**
 * uploader view.  displays an Acceptor component,
 */
class Uploader extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    window.scrollTo(0, 0)
  }
  /**
   * given an array of image files, uploaded through the Acceptor,
   * POST them to the server.
   * @param {[image files]} images
   */
  upload(images) {
    // allow the server to requet a resend
    this.props.socket.on('resend-images', () => 
    {
      this.upload(images)
    })

    // send images, with messages "start" "batch" (one batch per 10 images) "end"
    console.log("sending images: " + images.length)
    this.props.socket.emit("send-images-begin")
    for (let i = 0; i < images.length; i += 10) { 
      let batch = images.slice(i, i + 10)
      const batchNames = batch.map(i => i.name)
      console.log('sending batch with length: ' + batch.length)
      this.props.socket.emit('send-images', {'names': batchNames, 'images': batch})
    }
    console.log('sent all images, ')
    this.props.socket.emit("send-images-end")

    // we then wait for the images-saved event via sockets.  listener initialized in App.js
  }

  render() {
    return (
      <div className="page-view uploader">
        <FileDrop uploadFunc={(images) => { this.upload(images) }} socket={this.props.socket} />
      </div>
    )
  }
}

export default Uploader
