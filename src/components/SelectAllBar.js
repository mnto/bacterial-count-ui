import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';

import '../stylesheets/SelectAllBar.scss';

/**
 * the first row, which contains select all/none buttons
 * @props selectedMask bool[] a mask determining which files are selected
 * @props updateMask () : void updates the selected mask in the parent's state
 */
class SelectAllBar extends Component {
  /**
   * constructs a new mask, setting all values to true
   */
   constructor(props) {
     super(props);
     this.state = {
       selectAll: false
     }
   }

  selectAll = (e) => {
    e.preventDefault();
    const newMask = []
    for (let i = 0; i < this.props.selectedMask.length; i++)
      newMask.push(true)

    this.props.updateMask(newMask)
    this.setState({selectAll: true})
    console.log('select', this.state.selectAll);
  };

  /**
   * constructs a new mask, setting all values to false and checking the first file
   */
  selectOne = () => {
    const newMask = []

    newMask.push(true)
    for (let i = 1; i < this.props.selectedMask.length; i++)
      newMask.push(false)

    this.props.updateMask(newMask)
  }

  /**
   * constructs a new mask, setting all values to false
   */
  selectNone = (e) => {
    e.preventDefault();
    const newMask = []

    for (let i = 0; i < this.props.selectedMask.length; i++)
      newMask.push(false)

    this.props.updateMask(newMask);
    this.setState({selectAll: false});
    console.log('unselect', this.state.selectAll);
  }

  render() {
    const { selectAll } = this.state;
    return (
      <div className='SelectAllBar'>
        <Button className={ selectAll ? "unselect" : "select"} onClick={this.state.selectAll ? (e) => this.selectNone(e) : (e) => this.selectAll(e) }>{this.state.selectAll ? "Unselect All" : "Select All" }</Button>
        <Button className="select-results" onClick={() => {this.selectOne()}}>Select Results Only</Button>
      </div>
    )
  }
}

export default SelectAllBar
