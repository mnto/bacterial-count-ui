import React, { Component } from 'react'
import { Pagination } from 'semantic-ui-react'
import '../stylesheets/PaginationNavigation.scss';

export default class PaginationNavigation extends Component {
  state = {
    activePage: this.props.page,
    boundaryRange: 1,
    siblingRange: 1,
    showEllipsis: true,
    showFirstAndLastNav: false,
    showPreviousAndNextNav: true,
    totalPages: this.props.pages,
  }

  /**
   * method that runs when the props update
   * checks for differences and appropriately updates state
   * @param {*} nextProps the incoming props we're updating to
   */
  componentWillReceiveProps(nextProps) {
    if(nextProps.page !== this.state.activePage) {
      this.setState({ activePage: nextProps.page });
    }
  }

  handlePaginationChange = (e, context) => {
    console.log(JSON.stringify(context))
    const { activePage } = context
    console.log("pagination setting page to: " + (activePage - 1))
    this.props.setPage(activePage - 1)
    // this.setState({ activePage })
  }

  render() {
    const {
      activePage,
      boundaryRange,
      siblingRange,
      showEllipsis,
      showFirstAndLastNav,
      showPreviousAndNextNav,
      totalPages,
    } = this.state

    if (!this.props.hidden)
      return (
        <Pagination
          activePage={activePage}
          defaultActivePage={1}
          boundaryRange={boundaryRange}
          onPageChange={this.handlePaginationChange}
          siblingRange={siblingRange}
          totalPages={totalPages}
          pointing
          secondary
          // Heads up! All items are powered by shorthands, if you want to hide one of them, just pass `null` as value
          ellipsisItem={showEllipsis ? undefined : null}
          firstItem={showFirstAndLastNav ? undefined : null}
          lastItem={showFirstAndLastNav ? undefined : null}
          prevItem={showPreviousAndNextNav ? undefined : null}
          nextItem={showPreviousAndNextNav ? undefined : null}
        />
      )
    return null
  }
}
