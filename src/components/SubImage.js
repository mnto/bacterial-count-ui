import React, { Component } from 'react';

/**
 * class to display part of an image.
 * we're given a link to an image (the full image), and its width,height.
 * using this, and the bounds of a cropped subimage, we zoom the image in
 * by a factor of 200 / subimage_width, and position the subimage with negative margins based on the bounds.
 */
class SubImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 0
    }
  }

  componentDidMount() {
    console.log('did mount');
    const canvas = this.refs.canvas;
    const ctx = canvas.getContext("2d");
    const img = this.refs.image;

    let [ top, left, bottom, right ] = this.props.bounds;

    // get the dimensions of the subimage
    const width = right - left;
    const height = bottom - top;

    // image width stays a constant, and calculates the height based on width:height ratio
    this.setState({height: (150 / width) * height});

    // crop and zoom in on image
    img.onload = () => {
      ctx.drawImage(img, left, top, width, height, 0, 0, 150, (150 / width) * height)
    }
  }

  render() {
    return (
      <div>
        <canvas ref="canvas" width={150} height={this.state.height} />
        <img alt="cellCluster" ref="image" src={this.props.src} style={{'display': 'none'}} />
      </div>
    )
  }
}

export default SubImage;
