import React, { Component } from 'react';
import SelectAllBar from './SelectAllBar.js';
import ResultFileList from './ResultFileList.js';
import DownloadButton from './DownloadButton.js';

import '../stylesheets/ResultsView.scss';

class ResultsView extends Component {

  /**
   * read directory of files
   * @returns list of filenames that can be downloaded
   */
  getDownloadableFiles() {
    // return
  }

  /**
   * processes the mask and returns a list of selected filenames
   */
  getSelectedFiles() {
    let r = []
    for (let i = 0; i < this.state.filenames.length; i++)
      if (this.state.selectedMask[i])
        r.push(this.state.filenames[i])
    return r
  }

  state = {
    selectedMask: [false, false, false],
    filenames: ['Results table (.csv)', 'Image with labelled regions', 'Cropped images of clusters'],
  }

  render() {
    return (
      <div className="page-view">
        <p className="header-text">We've updated your counts accordingly</p>
        <SelectAllBar
          selectedMask={ this.state.selectedMask }
          updateMask= { (mask) => {
            this.setState({ selectedMask: mask })
          } }
          />
        <ResultFileList
          files={ this.state.filenames }
          selectedMask={ this.state.selectedMask }
          updateMask={ (mask) => this.setState({ selectedMask: mask }) }
          />
        <DownloadButton
          getFiles={ () => { return this.getSelectedFiles() } }
          />
      </div>
    )
  }
}

export default ResultsView
